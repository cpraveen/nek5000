To try the program on test.geo do following

gmsh -2 test.geo
python test.py

This creates test.rea file.

Some other examples:

karman: 
cylinder with transfinite mesh. Cylinder is make into circle.

karman2: 
cylinder with unstructured mesh of 8 node quads. This produces all edges to be of type 'm'
